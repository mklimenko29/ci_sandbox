#include "gtest/gtest.h"
#include "../src/lib.hpp"
#include "../src/data.hpp"

TEST(library, Add) {
	for (std::size_t i = 0; i < 10; ++i) {
		ASSERT_EQ(lib::Add(i, i * 2), i * 3);
	}
}

TEST(data, Add) {
	ASSERT_EQ(((data::second + data::third) > 3 ? (data::second + data::third) : 2), 2);
}
//TEST(library, Fail) {
//	for (std::size_t i = 0; i < 10; ++i) {
//		ASSERT_EQ(lib::Add(i, i * 2), i * 4);
//	}
//}
